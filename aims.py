#!/usr/bin/env python
import numpy as np

def main():
    print "Welcome to the AIMS module"


#function that computes the standard deviation of a list of numbers
def std(nums_lst):
    try:           
        avg_val = np.average(nums_lst)                                       #Get the average
        sum_n = 0                                                            # Set the sum to zero

        for n in nums_lst:                                                   # iterate through the list
            diff = n - avg_val                                               # the difference between each value and the average
            sum_n =  sum_n + diff**2                                         # obtain the sum

        std_d = np.sqrt(sum_n/len(nums_lst))                                 # compute the standard deviation
        std_round = round(std_d,5)

    except TypeError:
        raise TypeError("The List provided was not a list of numbers")       # display in case of the inputing wrong data type
   
    except ZeroDivisionError:
        raise ZeroDivisionError("The List provided was Empty")               # display in case of division by zero

    return std_round                                                         # return the result


#function that computes the average range in the file list
def avg_range(files_lst):    
    if files_lst:                                                
        try:
            range_vals = []                                                      # define the list that will contain the ranges
            for file in files_lst:                                               # iterate through the list of files
                the_file = open(file)                                            # open the file
                for line in the_file:                                            # read line by line
                    if line.startswith('Range'):                                 # check if the line starts with "Range"
                        split_lst = [word.strip() for word in line.split(':')]   # if so, Split the line basing on the :
                        range_vals.append(int(split_lst[1]))                     # append valueat second index to the list
                the_file.close()                                                 # close the file
            Avg_value = np.average(range_vals)                                   # compute the average of the range values in the list

        except IOError:
            raise IOError("There is no such file or Directory")                  # Display in case file name not found
        
        except TypeError:
            raise TypeError("Expected input should be a list")                   # Display in case of  wrong datatype
    
    else:
        print 'warning: List provided was empty, pleases'
        Avg_value = 0.0

    return Avg_value                                                         # return the average value


if __name__ == "__main__":
    main()
