from nose.tools import assert_equal

import aims

#############################Tests for the std() ######################################################################

# Testing a list of negative values
def test_negatives():

    nums_lst = [-1,-6,-2,-4]
    obs = aims.std(nums_lst)
    exp = 1.92029
    assert_equal(obs, exp)

# testing a list of mixture of integers and floats
def test_intwith_floats():

    nums_lst = [3.0,4,6,0.5,1.5]
    obs = aims.std(nums_lst)
    exp = 1.92354
    assert_equal(obs, exp)


# testing a list of both negative and positive numbers
def test_pos_negs():

    nums_lst = [-1,-2,4,2]
    obs = aims.std(nums_lst)
    exp = 2.38485
    assert_equal(obs, exp)

#Testing a list of positive integers
def test_positives():
    nums_lst = [0, 2, 4 ]
    obs = aims.std(nums_lst)
    exp = 1.63299
    assert_equal(obs, exp)


###########################Tests for the average range function#########################################################

# Testing for correctness of the output
def test_files_list():

    files_lst = ['data/bert/audioresult-00317','data/bert/audioresult-00215']
    obs = aims.avg_range(files_lst)
    exp = 5.5
    assert_equal(obs, exp)
